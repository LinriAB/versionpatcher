// -------------------------------------------------------
//
// VersionPatcher
// ==============
//
// Free for all to use, adapt and share both in binary and
// source code form. Request changes, contribute or fork
// from gitlab.com/LinriAB/VersionPatcher.
//
// If any part of this source code is used for adaption
// or extension, I ask only that references to my contact
// details below and this remark remain intact. You're
// welcome to add your own information as a contributor.
//
// Rickard Engberg
// CopyRight: 2022 Rickard Engberg
// Email:     rickard@engberg.io
// Web site:  www.linri.se
//            www.codingcleaner.com (coding discussions)
//
// -------------------------------------------------------

unit hlp.Types;

interface

type
	TVersion = type string;

	TStringHelper = record helper for string
	public
		function Equals( const AString : string ) : boolean;
		function Left( const ALength : integer ) : string;
		function CopyTo( const ASubString : string; const AInclusive : boolean = true ) : string;
		function CopyBetween( const AFrom, ATo : string; const AInclusive : boolean = true ) : string;
		function CopyFrom( const ASubString : string; const AInclusive : boolean = true ) : string; overload;
		function CopyFrom( const AIndex : integer ) : string; overload;

	end;

	TVersionHelper = record helper for TVersion
	public
		function IncBuild : TVersion;
		function IncRelease : TVersion;
		function IncMinor : TVersion;
		function IncMajor: TVersion;
		function AsCommaSeparated : string;

	end;


implementation

uses
  SysUtils, StrUtils;

function TStringHelper.Equals( const AString : string ) : boolean;
begin

	Result := CompareText( Self, AString ) = 0;

end;

function TStringHelper.Left( const ALength : integer ) : string;
begin

	Result := Copy( Self, 1, ALength );

end;

function TStringHelper.CopyTo( const ASubString : string; const AInclusive : boolean = true ) : string;
var
	nPos : integer;
begin

	if Pos( ASubString, Self ) > 0 then
	begin

		if AInclusive then
			nPos := Pos( ASubString, Self ) + Length( ASubString )
		else
			nPos := Pos( ASubString, Self ) - 1;

		Result := Copy( Self, 1, nPos );

	end
	else
		Result := Self;

end;

function TStringHelper.CopyBetween( const AFrom, ATo : string; const AInclusive : boolean = true ) : string;
begin

	Result := Self.CopyFrom( AFrom, AInclusive ).CopyTo( ATo, AInclusive );

end;

function TStringHelper.CopyFrom( const ASubString : string; const AInclusive : boolean = true ) : string;
var
	nPos : integer;
begin

	if Pos( ASubString, Self ) > 0 then
	begin

		if AInclusive then
			nPos := Pos( ASubString, Self )
		else
			nPos := Pos( ASubString, Self ) + Length( ASubString );

		Result := Copy( Self, nPos );

	end
	else
		Result := Self;

end;

function TStringHelper.CopyFrom( const AIndex : integer ) : string;
begin

	Result := Copy( Self, AIndex + 1, Length( Self ) - AIndex );

end;

function TVersionHelper.IncBuild : TVersion;
begin

	var t := SplitString( Self, '.' );
	while Length( t ) < 4 do
		t := t + [ '0' ];

	Self := Format(
		'%s.%s.%s.%s',
		[
			t[ 0 ],
			t[ 1 ],
			t[ 2 ],
			IntToStr( StrToIntDef( t[ 3 ], 0 ) + 1 )
		] );

	Result := Self;

end;

function TVersionHelper.IncRelease : TVersion;
begin

	var t := SplitString( Self, '.' );
	while Length( t ) < 4 do
		t := t + [ '0' ];

	Self := Format(
		'%s.%s.%s.%s',
		[
			t[ 0 ],
			t[ 1 ],
			IntToStr( StrToIntDef( t[ 2 ], 0 ) + 1 ),
			IntToStr( StrToIntDef( t[ 3 ], 0 ) )
		] );

	Result := Self;

end;

function TVersionHelper.IncMinor : TVersion;
begin

	var t := SplitString( Self, '.' );
	while Length( t ) < 4 do
		t := t + [ '0' ];

	Self := Format(
		'%s.%s.%s.%s',
		[
			t[ 0 ],
			IntToStr( StrToIntDef( t[ 1 ], 0 ) + 1 ),
			'0',
			IntToStr( StrToIntDef( t[ 3 ], 0 ) )
		] );

	Result := Self;

end;

function TVersionHelper.IncMajor: TVersion;
begin

	var t := SplitString( Self, '.' );
	while Length( t ) < 4 do
		t := t + [ '0' ];

	Self := Format(
		'%s.%s.%s.%s',
		[
			IntToStr( StrToIntDef( t[ 0 ], 0 ) + 1 ),
			'0',
			'0',
			IntToStr( StrToIntDef( t[ 3 ], 0 ) )
		] );

	Result := Self;

end;

function TVersionHelper.AsCommaSeparated: string;
begin

	Result := StringReplace( Self, '.', ',', [ rfReplaceAll ] );

end;

end.
