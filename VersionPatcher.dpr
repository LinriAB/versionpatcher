// -------------------------------------------------------
//
// VersionPatcher
// ==============
//
// Free for all to use, adapt and share both in binary and
// source code form. Request changes, contribute or fork
// from gitlab.com/LinriAB/VersionPatcher.
//
// If any part of this source code is used for adaption
// or extension, I ask only that references to my contact
// details below and this remark remain intact. You're
// welcome to add your own information as a contributor.
//
// Rickard Engberg
// CopyRight: 2022 Rickard Engberg
// Email:     rickard@engberg.io
// Web site:  www.linri.se
//            www.codingcleaner.com (coding discussions)
//
// Contributors:
// =============
// <Space for contributors contact and/or recognition>
//
// Changes, adaptations and other notes
// ====================================
// 2022-04-07 0.9    Initial pre-release launch
//
// -------------------------------------------------------

program VersionPatcher;

{$APPTYPE CONSOLE}
{$R *.res}
{$R 'version.res' 'version.rc'}

uses
  System.SysUtils,
  System.IOUtils,
  System.DateUtils,
  cls.VersionFile in 'cls.VersionFile.pas',
  hlp.Types in 'hlp.Types.pas';

const
	OPER_DELETE_FILE = 'del';
	OPEN_PRODUCTVERSION = 'pv';
	OPER_ACTIVEFILE = 'f';
	OPER_COMPANY = 'cn';
	OPER_DESCRIPTION = 'd';
	OPER_FILEVERSION = 'fv';
	OPER_LEGAL_COPYRIGHT = 'lc';
	OPER_PRODUCTNAME = 'pn';
	OPER_TEMPLATEFILE = 'tf';
	TUPLE_MAJOR = 'major';
	TUPLE_MINOR = 'minor';
	TUPLE_RELEASE = 'release';

resourcestring
	SDoneHitEnterOnlyNeededWhenRunnin = 'Done, hit enter (only needed when running in debug mode)...';

procedure ShowTitle;
begin

	WriteLn;
	WriteLn( 'VersionPatcher' );
	WriteLn( '==============' );
	WriteLn( 'Usage: VersionPatcher -vf <versionfile> <operation> <operation/value> <operation> <operation/value> ...' );
	WriteLn;
	WriteLn( 'DONATIONS ARE APPRECIATED: Please donate a buck or two on paypal: rickard@engberg.io' );
	WriteLn;

end;

procedure ShowHelp;
begin

	WriteLn( 'Operation  -f <verfile>    Sets the version file to work on for the following operations' );
	WriteLn( '           -tf <template>  Select a file as a template version file to use' );
	WriteLn( '           -fv <1.2.3.4>   Set the file version for the active version file' );
	WriteLn( '           -fv build       Increase the build tuple' );
	WriteLn( '           -fv release     Increase the release tuple' );
	WriteLn( '           -fv minor       Increase the minor tuble, set release to 0' );
	WriteLn( '           -fv major       Increase the major tuble, set minor and release to 0' );
	WriteLn( '           -pv <1.2.3.4>   Set the file version for the active version file' );
	WriteLn( '           -pv build       Increase the build tuple' );
	WriteLn( '           -pv release     Increase the release tuple' );
	WriteLn( '           -pv minor       Increase the minor tuble, set release to 0' );
	WriteLn( '           -pv major       Increase the major tuble, set minor and release to 0' );
	WriteLn( '           -cn <company>   Set the company field' );
	WriteLn( '           -d <descr>      Set the file description field' );
	WriteLn( '           -lc <copyright> Set the legal copyright field' );
	WriteLn( '           -pn <prodname>  Set the product name field' );
  WriteLn( '           -del <filename> Attempts to delete <filename>' );
	WriteLn;

	WriteLn( 'VersionPatcher doesn''t really patch your binary, but the version.rc (or whatever you call' );
	WriteLn( 'it) file used by your maker to compile and link your project. That way you can set up your' );
	WriteLn( 'IDE to set specific build information, version number or version tuple increments' );
	WriteLn;
	WriteLn( 'Originaly authored by Rickard Engberg in 2022 after 30 years of pondering over versioning' );
	WriteLn( 'schemes. Free for all to use, adapt or throw out as is each ones leasure. If changing the code' );
	WriteLn( 'though, just be a sport and make a reference to me in the source and credits.' );
	WriteLn;
	WriteLn( 'By the way, your not obliged to, but I do accept donations on paypal. See email address below. A' );
	WriteLn( 'USD/EUR or two should be adequate, don''t you think.' );
	WriteLn;
	WriteLn( 'Also, I''ve posted the source openly on gitlab: https://gitlab.com/LinriAB/versionpatcher' );
	WriteLn;
	WriteLn( 'Sincerely' );
	WriteLn;
	WriteLn( 'Rickard Engberg' );
	WriteLn( 'rickard@engberg.io' );
	WriteLn;
	WriteLn;

end;

procedure doSetActiveFile( const AParam : string );
begin

	TVersionFile.VersionFileName := ExpandFileName( AParam );

	WriteLn( 'Version file:  ' + TVersionFile.VersionFileName );
	WriteLn( ' Created:      ' + DateTimeToStr( TVersionFile.CreatedTime ) );
	WriteLn( ' Modified:     ' + DateTimeToStr( TVersionFile.ModifiedTime ) );
	WriteLn( ' Accessed:     ' + DateTimeToStr( TVersionFile.AccessedTime ) );

end;

procedure doSetTemplateFile( const AParam : string );
begin

	TVersionFile.TemplateFileName := ExpandFileName( AParam );

	WriteLn( 'Template file: ' + TVersionFile.TemplateFileName );

end;

procedure doSetCompany( const ACompanyName : string );
begin

	var old := TVersionFile.Company;
	WriteLn( Format( ' -> Company:   %s (%s)', [ ACompanyName, old ] ) );
	TVersionFile.Company := ACompanyName;

end;

procedure doSetDescription( const ADescriptionName : string );
begin

	var old := TVersionFile.Description;
	WriteLn( Format( ' -> Descr:     %s (%s)', [ ADescriptionName, old ] ) );
	TVersionFile.Description := ADescriptionName;

end;

procedure doSetProductName( const AProductName : string );
begin

	var old := TVersionFile.ProductName;
	WriteLn( Format( ' -> Product:   %s (%s)', [ AProductName, old ] ) );
	TVersionFile.ProductName := AProductName;

end;

procedure doSetCopyright( const ACopyright : string );
begin

	var old := TVersionFile.Copyright;
	WriteLn( Format( ' -> Copy:      %s (%s)', [ ACopyright, old ] ) );
	TVersionFile.Copyright := ACopyright;

end;

procedure doSetFileVersion( const AFileVersion : string );
begin

	var old := TVersionFile.FileVersion;
	WriteLn( Format( ' -> FileVer:   %s (%s)', [ AFileVersion, old ] ) );
	TVersionFile.FileVersion := AFileVersion;

end;

procedure doSetProductVersion( const AProductVersion : string );
begin

	var old := TVersionFile.ProductVersion;
	WriteLn( Format( ' -> ProdVer:   %s (%s)', [ AProductVersion, old ] ) );
	TVersionFile.ProductVersion := AProductVersion;

end;

procedure doDeleteFile( const AFileName : string );
begin

	var fileName := ExpandFileName( AFileName );
	if not TFile.Exists( fileName ) then
  	exit;

	Write( Format( ' Delete file:  %s)', [ filename ] ) );

	var r := TryRun(
		function() : boolean
		begin

			try

				TFile.Delete( fileName );
				Result := not TFile.Exists( fileName );

			except

				Result := false;

			end;

		end );
	if not r then
		WriteLn( ' - FAILED' )
	else
    WriteLn;

end;

procedure DoTheWork;
begin

	for var i := 1 to ParamCount do
	begin

		if CharInSet(	ParamStr( i )[ 1 ], [ '-', '/' ] ) then
		begin

      var param := ParamStr( i ).CopyFrom( 1 );
			var paramVal := ParamStr( i + 1 );

			if param.Equals( OPER_ACTIVEFILE ) then
				doSetActiveFile( paramVal )

			else if param.Equals( OPER_TEMPLATEFILE ) then
				doSetTemplateFile( paramVal )

			else if param.Equals( OPER_COMPANY ) then
				doSetCompany( paramVal )

			else if param.Equals( OPER_PRODUCTNAME ) then
				doSetProductName( paramVal )

			else if param.Equals( OPER_LEGAL_COPYRIGHT ) then
				doSetCopyright( paramVal )

			else if param.Equals( OPER_DESCRIPTION ) then
				doSetDescription( paramVal )

			else if param.Equals( OPER_FILEVERSION ) then
			begin

				var newVersion : TVersion := TVersionFile.FileVersion;
				if paramVal.Equals( TUPLE_RELEASE ) then
					newVersion := newVersion.IncRelease
				else if paramVal.Equals( TUPLE_MINOR ) then
					newVersion := newVersion.IncMinor
				else if paramVal.Equals( TUPLE_MAJOR ) then
					newVersion := newVersion.IncMajor;
				doSetFileVersion( TVersion( paramVal ) )

			end

			else if param.Equals( OPEN_PRODUCTVERSION ) then
			begin

				var newVersion : TVersion := TVersionFile.ProductVersion;
				if paramVal.Equals( TUPLE_RELEASE ) then
					newVersion := newVersion.IncRelease
				else if paramVal.Equals( TUPLE_MINOR ) then
					newVersion := newVersion.IncMinor
				else if paramVal.Equals( TUPLE_MAJOR ) then
					newVersion := newVersion.IncMajor;
				doSetProductVersion( TVersion( paramVal ) )

			end

			else if param.Equals( OPEN_PRODUCTVERSION ) then
				doSetProductVersion( paramVal )

			else if param.Equals( OPER_DELETE_FILE ) then
        doDeleteFile( paramVal );

		end;

	end;

end;

begin

	ShowTitle;
	if ParamCount = 0 then
		ShowHelp;
	DoTheWork;

end.
