// -------------------------------------------------------
//
// VersionPatcher
// ==============
//
// Free for all to use, adapt and share both in binary and
// source code form. Request changes, contribute or fork
// from gitlab.com/LinriAB/VersionPatcher.
//
// If any part of this source code is used for adaption
// or extension, I ask only that references to my contact
// details below and this remark remain intact. You're
// welcome to add your own information as a contributor.
//
// Rickard Engberg
// CopyRight: 2022 Rickard Engberg
// Email:     rickard@engberg.io
// Web site:  www.linri.se
//            www.codingcleaner.com (coding discussions)
//
// -------------------------------------------------------

unit cls.VersionFile;

interface

uses
	System.SysUtils, System.IOUtils, System.StrUtils,

	hlp.Types;

type
	TVersionFile = class
	strict private
	class var
		FTemplateFileName, FVersionFileName : string;
		FLines : TArray<string>;

		class function getCompany : string; static;
		class function getCopyright : string; static;
		class function getDescription : string; static;
		class function getFileVersion : TVersion; static;
		class function getProductName : string; static;
		class function getProductVersion : TVersion; static;
		class procedure setCompany( const ACompany : string ); static;
		class procedure setCopyright( const ACopyright : string ); static;
		class procedure setDescription( const ADescription : string ); static;
		class procedure setFileVersion( const AVersion : TVersion ); static;
		class procedure setProductName( const AProductName : string ); static;
		class procedure setProductVersion( const AVersion : TVersion ); static;
		class procedure setVersionFile( const AVersionFileName : string ); static;

		class function doFindDefineLineIdx( const AName : string ) : integer;
		class function doGetDefineValue( const ALineIdx : integer; const AQuoted : boolean ) : string;
		class procedure doLoadVersionFile;
		class procedure doSaveVersionFile;
		class procedure doSetDefineValue( const ALineIdx : integer; const AValue : string; const AQuoted : boolean );

	public
		class function CreatedTime : TDateTime;
		class function ModifiedTime : TDateTime;
		class function AccessedTime : TDateTime;

		class property VersionFileName : string read FVersionFileName write setVersionFile;
		class property TemplateFileName : string read FTemplateFileName write FTemplateFileName;
		class property Company : string read getCompany write setCompany;
		class property Description : string read getDescription write setDescription;
		class property ProductName : string read getProductName write setProductName;
		class property Copyright : string read getCopyright write setCopyright;

		class property FileVersion : TVersion read getFileVersion write setFileVersion;
		class property ProductVersion : TVersion read getProductVersion write setProductVersion;

	end;

function TryRun( const AProc : TFunc<boolean>; const AAttempts : integer = 3; const AWaitForRetryMS : integer = 500 ) : boolean;

implementation

resourcestring
	SValueStartingPositionNotFound = 'Value starting position not found';
	SDefineNotFound = 'Define %s not found';
	SVersionFileNotFound = 'Version File not found';
	SVersionFileNameCanTBeEmpty = 'Version File Name can''t be empty';

const
	DEFINE = '#define';
	DEF_COMPANYNAME_STR = 'VER_COMPANYNAME_STR';
	DEF_DESCRIPTION_STR = 'VER_FILEDESCRIPTION_STR';
	DEF_COPYRIGHT_STR = 'VER_LEGALCOPYRIGHT_STR';
	DEF_PRODUCTNAME_STR = 'VER_PRODUCTNAME_STR';
	DEF_FILEVERSION = 'VER_FILEVERSION';
	DEF_FILEVERSION_STR = 'VER_FILEVERSION_STR';
	DEF_PRODUCTVERSION = 'VER_PRODUCTVERSION';
	DEF_PRODUCTVERSION_STR = 'VER_PRODUCTVERSION_STR';

function TryRun( const AProc : TFunc<boolean>; const AAttempts : integer = 3; const AWaitForRetryMS : integer = 500 ) : boolean;
begin

	Result := true;
	var attempt := 1;

	while ( attempt <= AAttempts ) do
	begin

		try

			Result := AProc;
			if Result then
      	exit;

		except on
			e: exception do
			begin

				inc( attempt );
        Sleep( AWaitForRetryMS );

			end;

		end;

	end;

end;

class function TVersionFile.getCompany : string;
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_COMPANYNAME_STR );
	Result := doGetdefineValue( lineIdx, true );

end;

class function TVersionFile.getCopyright : string;
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_COPYRIGHT_STR );
	Result := doGetdefineValue( lineIdx, true );

end;

class function TVersionFile.getDescription : string;
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_DESCRIPTION_STR );
	Result := doGetdefineValue( lineIdx, true );

end;

class function TVersionFile.getFileVersion : TVersion;
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_FILEVERSION_STR );
	Result := doGetdefineValue( lineIdx, true );

end;

class function TVersionFile.getProductName : string;
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_PRODUCTNAME_STR );
	Result := doGetdefineValue( lineIdx, true );

end;

class function TVersionFile.getProductVersion : TVersion;
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_PRODUCTVERSION_STR );
	Result := doGetdefineValue( lineIdx, true );

end;

class procedure TVersionFile.setCompany( const ACompany : string );
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_COMPANYNAME_STR );
	doSetDefineValue( lineIdx, ACompany, true );
	doSaveVersionFile;

end;

class procedure TVersionFile.setCopyright( const ACopyright : string );
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_COPYRIGHT_STR );
	doSetDefineValue( lineIdx, ACopyright, true );
	doSaveVersionFile;

end;

class procedure TVersionFile.setDescription( const ADescription : string );
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_DESCRIPTION_STR );
	doSetDefineValue( lineIdx, ADescription, true );
	doSaveVersionFile;

end;

class procedure TVersionFile.setFileVersion( const AVersion : TVersion );
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_FILEVERSION_STR );
	doSetDefineValue( lineIdx, AVersion, true );
	lineIdx := doFindDefineLineIdx( DEF_FILEVERSION );
	doSetDefineValue( lineIdx, AVersion.AsCommaSeparated, false );
	doSaveVersionFile;

end;

class procedure TVersionFile.setProductName( const AProductName : string );
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_PRODUCTNAME_STR );
	doSetDefineValue( lineIdx, AProductName, true );
	doSaveVersionFile;

end;

class procedure TVersionFile.setProductVersion( const AVersion : TVersion );
begin

	doLoadVersionFile;
	var lineIdx := doFindDefineLineIdx( DEF_PRODUCTVERSION_STR );
	doSetDefineValue( lineIdx, AVersion, true );
	lineIdx := doFindDefineLineIdx( DEF_PRODUCTVERSION );
	doSetDefineValue( lineIdx, AVersion.AsCommaSeparated, false );
	doSaveVersionFile;

end;

class procedure TVersionFile.setVersionFile( const AVersionFileName : string );
begin

	Assert( AVersionFileName > '', SVersionFileNameCanTBeEmpty );
	Assert( TFile.Exists( AVersionFileName ), SVersionFileNotFound );

	var fileName := AVersionFileName;

	if TFile.Exists( FTemplateFileName ) then
		TFile.Copy(
			FTemplateFileName,
			fileName,
			true );

	FVersionFileName := AVersionFileName;

end;

class function TVersionFile.doFindDefineLineIdx( const AName : string ) : integer;
begin

	Result := -1;

	for var i := 0 to Length( FLines ) - 1 do
	begin

		if StartsText( DEFINE + ' ' + AName + ' ', FLines[ i ] ) then
			exit( i );

	end;

	Assert( false, Format( SDefineNotFound, [ AName ] ) );

end;

class function TVersionFile.doGetDefineValue( const ALineIdx : integer; const AQuoted : boolean ) : string;
var
	line : string;
begin

	line := Trim( FLines[ ALineIdx ] );
	case AQuoted of
		true :
			result := StringReplace( line.CopyBetween( '"', '"', false ), '\0', '', [ rfReplaceAll ] );

		false :
			begin

				var valBegins := Length( line );
				while line[ valBegins ] <> #32 do
					dec( valBegins );
				Assert( valBegins >= 0, SValueStartingPositionNotFound );

				Result := Copy( line, valBegins + 1, Length( line ) - valBegins );

			end;

	end;

end;

class procedure TVersionFile.doLoadVersionFile;
begin

	TryRun(
		function() : boolean
		begin

			FLines := TFile.ReadAllLines( FVersionFileName );
			Result := true;

		end );

end;

class procedure TVersionFile.doSaveVersionFile;
begin

	TryRun(
		function() : boolean
		begin

			TFile.WriteAllLines( FVersionFileName, FLines );
			Result := true;

		end );

end;

class procedure TVersionFile.doSetDefineValue( const ALineIdx : integer; const AValue : string; const AQuoted : boolean );
var
	line : string;
begin

	line := Trim( FLines[ ALineIdx ] );
	case AQuoted of
		true :
			line := line.CopyTo( '"', false ) + Format( '"%s\0"', [ AValue ] );

		false :
			begin

				var valBegins := Length( line );
				while line[ valBegins ] <> #32 do
					dec( valBegins );
				Assert( valBegins >= 0, SValueStartingPositionNotFound );

				line := line.Left( valBegins ) + AValue;

			end;

	end;
  FLines[ ALineIdx ] := line;

end;

class function TVersionFile.CreatedTime : TDateTime;
begin

	Result := TFile.GetCreationTime( FVersionFileName );

end;

class function TVersionFile.ModifiedTime : TDateTime;
begin

	Result := TFile.GetLastWriteTime( FVersionFileName );

end;

class function TVersionFile.AccessedTime : TDateTime;
begin

	Result := TFile.GetLastAccessTime( FVersionFileName );

end;

end.
